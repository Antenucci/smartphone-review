package com.example.michele.esame;


import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class loginFragment extends Fragment {

     // private static final String URL = "http://192.168.1.102/servizi/login";
    private static final String URL = "http://micheleantenucci01.altervista.org/servizi/login";


    private static final String key_username = "username";
    private static final String key_password = "password";

    private RequestQueue codaRichiesta = null;





    private Button entra;
    private Button indietro;
    private EditText username_text;
    private EditText password_text;



    public loginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
      final   View view = inflater.inflate(R.layout.fragment_login, container, false);






        this.entra = (Button) view.findViewById(R.id.loginBtn);
        this.indietro = (Button) view.findViewById(R.id.indietroLgn);
        this.username_text = (EditText) view.findViewById(R.id.username);
        this.password_text = (EditText) view.findViewById (R.id.password);



        indietro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager f1 = getFragmentManager();
                android.app.FragmentTransaction fragmentTransaction = f1.beginTransaction();
                fragmentTransaction.hide(loginFragment.this);
                welcomeFragment welcomeFragment = new welcomeFragment();
                fragmentTransaction.add(R.id.welcome_frame, welcomeFragment);
                fragmentTransaction.commit();

            }
        });

        entra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String username = username_text.getText().toString().trim();
                final String password = password_text.getText().toString().trim();

                if (username.equals("") || password.equals("")) {

                    Toast.makeText(getActivity(), "Riempi tutti i campi", Toast.LENGTH_LONG).show();

                } else {


                    StringRequest richiesta_login = new StringRequest(Request.Method.POST, URL,

                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {

                                    if (response.trim().equals("utente non trovato")) {
                                        Toast.makeText(getActivity(), "Username o password errati", Toast.LENGTH_LONG).show();


                                    } else {

                                        Intent login = new Intent(getActivity(),menuActivity.class);
                                        login.putExtra(key_username, username);
                                        startActivity(login);
                                        getActivity().finish();

                                    }

                                }
                            },

                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(getActivity(), "Errore di connessione", Toast.LENGTH_LONG).show();
                                }
                            }
                    ){
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> map = new HashMap<>();
                            map.put(key_username, username);
                            map.put(key_password, password);
                            return map;
                        }
                    };

                    codaRichiesta = Volley.newRequestQueue(getActivity());
                    codaRichiesta.add(richiesta_login);

                }

            }
        }


        );




        return view;
    }






}
