package com.example.michele.esame;


import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;


import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Michele on 16/10/2017.
 */

public class smartphoneListFragment extends Fragment implements SearchView.OnQueryTextListener{



    private String URL;
    private String URL1;


    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private adapterSmartphoneList AdapterSmartphoneCardView;

    private ArrayList<smartphone> data_list = null;


   private RequestQueue codaRichiesta = null;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_smartphonelist, container, false);
        // return super.onCreateView(inflater, container, savedInstanceState);


        data_list = new ArrayList<>();

        //inserimento della RecyclerView
        this.recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        this.AdapterSmartphoneCardView = new adapterSmartphoneList(data_list, getActivity());

        this.layoutManager = new LinearLayoutManager(getActivity());
        this.recyclerView.setLayoutManager(layoutManager);
        this.recyclerView.setAdapter(AdapterSmartphoneCardView);

        this.setHasOptionsMenu(true);//permette di accedere alla toolbar dell'activity


        //this.URL = "http://10.0.2.2/servizi/visualizza";
     this.URL1 ="http://192.168.1.102/servizi/visualizza";


         JsonArrayRequest requeste = new JsonArrayRequest(URL1, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                for (int i = 0; i < response.length(); i++) {
                    try {

                        JSONObject obj = response.getJSONObject(i);
                        smartphone dati = new smartphone(
                                obj.getString("marca"),
                                obj.getString("nome"),
                                obj.getString("descrizione"),
                                obj.getString("immagine"));


                        data_list.add(dati);

                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }


                }
            }
        },


                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(), "Errore di connessione", Toast.LENGTH_LONG).show();
                    }
                }
        );





        this.codaRichiesta = Volley.newRequestQueue(getActivity());
        this.codaRichiesta.add(requeste);

        return view;
    }


//metodi della barra di ricerca
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {


//attacchiamo la searchView
        inflater.inflate(R.menu.menu, menu);


        final MenuItem menu_search = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu_search);



        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {//qviene richaimato quando si preme invio
           searchView.clearFocus();//la tastiera scompare e la selezione della barra scompare
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {//viene richiamato quando si inserisce il testo nella barra
                query = query.toLowerCase();
                ArrayList<smartphone> newList = new ArrayList<>();
                for (com.example.michele.esame.smartphone smartphone : data_list) {
                    String name = smartphone.getNome().toLowerCase();
                    String marca = smartphone.getMarca().toLowerCase();
                    if (name.contains(query)) {
                        newList.add(smartphone);


                    } else if (marca.contains(query)) {
                        newList.add(smartphone);
                    }

                }

               AdapterSmartphoneCardView.setFilter(newList);


                return true;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }
}