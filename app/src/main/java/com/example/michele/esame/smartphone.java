package com.example.michele.esame;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Michele on 13/10/2017.
 */

public class smartphone {



    private String nome;
    private String marca;
    private String descrizione;
    private String image_link;



    public smartphone(String pMarca, String pNome, String pDescrizione, String pImage_link) {
    
        this.nome = pNome;
        this.image_link = pImage_link;
        this.marca = pMarca;
        this.descrizione = pDescrizione;

    }




    public String getNome() {
        return nome;
    }

    public void setNome(String pDescription) {

        this.nome = pDescription;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String pMarca) {

        this.marca = pMarca;
    }

    public String getImage_link() {

        return image_link;
    }

    public void setImage_link(String pImage_link) {

        this.image_link = pImage_link;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String pDescrizione) {
        this.descrizione = pDescrizione;
    }
}