package com.example.michele.esame;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Michele on 17/10/2017.
 */

public class adapterSmartphoneList extends RecyclerView.Adapter<adapterSmartphoneList.ViewHolder> {

    ArrayList<smartphone> list;
    Context context;



    public adapterSmartphoneList(ArrayList<smartphone> list, Context context){
        this.list =list;
        this.context=context;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final smartphone list =  this.list.get(position);


        holder.textViewBrand.setText(this.list.get(position).getMarca());//permette di inserire gli elementi nella card view
        holder.textViewName.setText(this.list.get(position).getNome());
        holder.textViewDescription.setText(this.list.get(position).getDescrizione());
        Picasso.with(context).load(list.getImage_link()).into(holder.imageView);//permette di disegnare l'immagine


        holder.RelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(context,smartphoneActivity.class);
                intent.putExtra("nome",list.getNome());
                intent.putExtra("marca",list.getMarca());
                intent.putExtra("descrizione",list.getDescrizione());
                intent.putExtra(("immagine"),list.getImage_link());
                context.startActivity(intent);
               // Toast.makeText(context,"hai cliccato su " +list.getNome(),Toast.LENGTH_LONG).show();
            }
        });


        //holder.imageView.setText(list.getImage_link());
        //holder.textViewName.setText(list.getNome());
        //holder.textViewBrand.setText(list.getMarca());
    }



    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;
        private TextView textViewName;
        private TextView textViewBrand;
        private TextView textViewDescription;
        private RelativeLayout RelativeLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            imageView = (ImageView) itemView.findViewById(R.id.img_smartphone);
            textViewName = (TextView) itemView.findViewById(R.id.name_smartphone);
            textViewBrand = (TextView) itemView.findViewById(R.id.brand_smartphone);
            textViewDescription = (TextView) itemView.findViewById(R.id.description_smartphone);
            RelativeLayout = (RelativeLayout) itemView.findViewById(R.id.page_smartphone);//serve per fare il setOnclick

        /*    itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    int position = getAdapterPosition();

                    Snackbar.make(v, "Click detected on item " + position,
                            Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                }
            });
            */

        }


    }

    //serve per la barra di ricerca per filtraggio
public void setFilter(ArrayList<smartphone> smartphones){
    list = new ArrayList<>();
    list.addAll(smartphones);
    notifyDataSetChanged();//aggiorno i dati di ricerca
}

}
