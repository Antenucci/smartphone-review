package com.example.michele.esame;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class userComments extends AppCompatActivity {

    private RequestQueue codaRichiesta = null;


    private ArrayAdapter<String> adapter;

    private String URL;

    private GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_user_comments);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                finish();
            }
        });


        final String user = getIntent().getStringExtra("nome");




     //  this.URL = "http://10.0.2.2/servizi/commento_utente/"+user;
       this.URL ="http://192.168.1.53/servizi/commento_utente/"+user;

        Response.Listener<JSONArray> getListener = new Response.Listener<JSONArray>()

        {

            @Override
            public void onResponse(JSONArray response) {


                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject obj = response.getJSONObject(i);

                        adapter.add(obj.getString("smartphone"));
                        adapter.add(obj.getString("commento"));


                    } catch (JSONException e) {
                    }
                }

            }

        }

                ;

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError err) {
              finish();
                Toast.makeText(userComments.this, "Non hai inserito commenti", Toast.LENGTH_SHORT).show();

            }
        };


        adapter = new ArrayAdapter<String>(this, R.layout.row_comment,R.id.rowGrid);
        gridView = (GridView) findViewById(R.id.gridComment);
        gridView.setAdapter(adapter);



        codaRichiesta = Volley.newRequestQueue(this);

        JsonArrayRequest richiesta = new JsonArrayRequest(URL, getListener,errorListener);
        codaRichiesta.add(richiesta);

    }

}
