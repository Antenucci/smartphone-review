package com.example.michele.esame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class splashActivity extends AppCompatActivity {

int timeout=2000;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Thread thread = new Thread(){

    public void run(){
        try{
            sleep(timeout);
        }catch(InterruptedException e){
            e.printStackTrace();
        }finally{

            Intent intent = new Intent(splashActivity.this, mainActivity.class);
           startActivity(intent);

        }
    }

};
thread.start();
}

    @Override
    protected void onStop() {
        //TODO Auto-generated method stub
        super.onStop();
        finish();
    }



}

