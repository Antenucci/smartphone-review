package com.example.michele.esame;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Michele on 16/10/2017.
 */

public class welcomeFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_welcome,container,false);

        final Button login = (Button) view.findViewById(R.id.buttonLogin);
        final Button registrati = (Button) view.findViewById(R.id.buttonRegistrati);

        final TextView text1 = (TextView) view.findViewById(R.id.text);
        final TextView text2 =  (TextView) view.findViewById(R.id.textRegistrazione);

       login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager f1 = getFragmentManager();
                android.app.FragmentTransaction fragmentTransaction = f1.beginTransaction();
                fragmentTransaction.hide(welcomeFragment.this);
                loginFragment loginFragment = new loginFragment();
                fragmentTransaction.add(R.id.login_frame, loginFragment);
                fragmentTransaction.commit();



            }
        });


        registrati.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager f1 = getFragmentManager();
                android.app.FragmentTransaction fragmentTransaction = f1.beginTransaction();
                fragmentTransaction.hide(welcomeFragment.this);
                registerFragment registerFragment = new registerFragment();
                fragmentTransaction.add(R.id.register_frame, registerFragment);
                fragmentTransaction.commit();


            //    FragmentManager fragmentManager = getFragmentManager();
              //  fragmentManager.beginTransaction().replace(R.id.register_frame, new RegisterFragment()).commit();
            }
        });


        return view;
    }
}
