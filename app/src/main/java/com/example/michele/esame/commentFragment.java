package com.example.michele.esame;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Michele on 20/10/2017.
 */

public class commentFragment extends Fragment {



    private RequestQueue codaRichiesta = null;


    private ArrayAdapter<String> adapter = null;

    private GridView gridView = null;

    private String URL;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_comment, container, false);


        final String nome_smartphone = getActivity().getIntent().getStringExtra("nome");


        this.URL = "http://10.0.2.2/servizi/commento/"+nome_smartphone;

       // this.URL = "http://192.168.1.102/servizi/commento/"+nome_smartphone;

         Response.Listener<JSONArray> getListener = new Response.Listener<JSONArray>()

         {

             @Override
             public void onResponse(JSONArray response) {


                     for (int i = 0; i < response.length(); i++) {
                         try {

                             JSONObject obj = response.getJSONObject(i);
                             adapter.add(obj.getString("utente"));
                             adapter.add(obj.getString("commento"));

                         } catch (JSONException e) {
                         }
                     }

                 }

             }

             ;

             Response.ErrorListener errorListener = new Response.ErrorListener() {
                 @Override
                 public void onErrorResponse(VolleyError err) {
                     getActivity().finish();
                     Toast.makeText(getActivity(), "Non ci sono commenti del " + nome_smartphone, Toast.LENGTH_SHORT).show();

                 }
             };


        adapter = new ArrayAdapter<String>(getActivity(), R.layout.row_comment,R.id.rowGrid);
        gridView = (GridView) view.findViewById(R.id.gridComment);
        gridView.setAdapter(adapter);



        codaRichiesta = Volley.newRequestQueue(getActivity());

        JsonArrayRequest richiesta = new JsonArrayRequest(URL, getListener,errorListener);
        codaRichiesta.add(richiesta);


        return view;
    }
}
