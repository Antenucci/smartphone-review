package com.example.michele.esame;

import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;


public class menuActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{




    smartphoneListFragment home = new smartphoneListFragment();
    FragmentManager fm = getFragmentManager();



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        TextView txtProfileName = (TextView) navigationView.getHeaderView(0).findViewById(R.id.UsernameTextMenu);

        String name = getIntent().getStringExtra("username");
        txtProfileName.setText("Ciao " + name);//permette di far visualizzare l'username nel munu principale



        fm.beginTransaction().add(R.id.content_frame_menu, home).commit();


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds list to the action bar if it is present.
     //  getMenuInflater().inflate(R.menu.menu, menu);
       // MenuItem menu_search = menu.findItem(R.id.action_search);
        //SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu_search);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
     //   if (id == R.id.action_settings) {
       //     Intent info = new Intent(MenuActivity.this,InfoActivity.class);
         //   startActivity( info);
           // return true;
        //}

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        String nameInsert = getIntent().getStringExtra("username");


        int id = item.getItemId();


        if (id == R.id.nav_inserisci) {

            Intent insert = new Intent(menuActivity.this,insertActivity.class);

            insert.putExtra("nome",nameInsert);
            startActivity(insert);



        } else if (id == R.id.nav_visualizza_post) {


            Intent show_comment = new Intent(menuActivity.this,userComments.class);

            show_comment.putExtra("nome",nameInsert);
            startActivity(show_comment);



        } else if (id == R.id.nav_info) {

            Intent info = new Intent(menuActivity.this,infoActivity.class);
            startActivity(info);


        }
            else if (id == R.id.nav_inserisci) {

            Intent insert = new Intent(menuActivity.this,insertActivity.class);

            insert.putExtra("nome",nameInsert);
            startActivity(insert);

        } else if(id == R.id.nav_logout){

            AlertDialog.Builder alert = new AlertDialog.Builder(menuActivity.this);



            alert.setMessage("Vuoi uscire?");

            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            Intent back = new Intent(menuActivity.this,mainActivity.class);
                            startActivity(back);
                             finish();
                        }
                    });

                    alert.setNegativeButton("Annulla", null);
            alert.show();


        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);


        return true;


    }

    public MenuInflater getSupportMenuInflater() {
        return getSupportMenuInflater();
    }

    //cambio di card view (non funziona)
/*
    private void displayView(int position) {



        switch (position) {
            case 0:
               SmartphoneListFragment home = new SmartphoneListFragment();
                break;
            case 1:


            default:
                break;
        }

      if (home != null) {
          fm = getFragmentManager();
          fm.beginTransaction().add(R.id.content_frame_menu, home).commit();
      }
    }
*/

}
