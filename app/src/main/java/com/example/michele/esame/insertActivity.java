package com.example.michele.esame;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;




public class insertActivity extends AppCompatActivity {


   //private final static  String URL = "http://10.0.2.2/servizi/inserimento";
    private final static  String URL = "http://192.168.1.53/servizi/inserimento";


    private RequestQueue codaRichiesta = null;



    private static final String key_name = "utente";
    private static final String key_commento = "commento";
    private static final String key_smartphone = "smartphone";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //permette di far inserire il pulsante back nella toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                finish();
            }
        });

       final Button inserisci_commento = (Button) findViewById(R.id.BtnInserisciCOmmentA);
     //  final Button indietro = (Button) findViewById(R.id.BtnIndietroCommentA);
        final Spinner spinner_smartphone = (Spinner) findViewById(R.id.spinner);
        final EditText commento = (EditText) findViewById(R.id.CommentoTextInsert);



        List<String> list = new ArrayList<>();

        list.add("iphone 7");
        list.add("galaxy s8");
        list.add("p9");
        list.add("p10");
        list.add("iphone X");
        list.add("iphone 8 plus");
        list.add("pixel 2");


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item,list);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);

        spinner_smartphone.setAdapter(adapter);


        final String username = getIntent().getStringExtra("nome");


        inserisci_commento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String username_string = username.toString().trim();
                final String commento_string = commento.getText().toString().trim();
                final String nome_smartphone_string = spinner_smartphone.getSelectedItem().toString().trim();

                if (commento_string.equals("") ) {

                    Toast.makeText(insertActivity.this, "Inserisci il commento", Toast.LENGTH_LONG).show();

                } else {

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,

                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {

                                    if (response.trim().equals("errore")) {
                                        Toast.makeText(insertActivity.this, "Commento già inserito", Toast.LENGTH_LONG).show();


                                    } else {
                                        Toast.makeText(insertActivity.this, "Commento inserito", Toast.LENGTH_LONG).show();





                                    }


                                }
                            },

                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(insertActivity.this, "Errore di connessione", Toast.LENGTH_LONG).show();
                                }
                            }
                    )
                    {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> map = new HashMap<>();

                            map.put(key_smartphone, nome_smartphone_string);
                            map.put(key_commento, commento_string);
                            map.put(key_name, username_string);

                            return map;
                        }
                    };

                    codaRichiesta = Volley.newRequestQueue(insertActivity.this);
                    codaRichiesta.add(stringRequest);

                    finish();
                }
            }
        });

    /*    indietro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        */
    }

}
