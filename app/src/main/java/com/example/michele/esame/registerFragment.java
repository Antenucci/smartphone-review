package com.example.michele.esame;


import android.app.FragmentManager;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class registerFragment extends Fragment {



    private Button conferma;
    private Button indietro;
    private EditText username;
    private EditText nome;
    private EditText cognome;
    private EditText confermaPassword;
    private EditText password;
    private EditText email;
    private RadioGroup radioSexGroup;
    private RadioButton radioSexButton;


    private static final String key_username = "username";
    private static final String key_password = "password";
    private static final String key_nome = "nome";
    private static final String key_cognome = "cognome";
    private static final String key_email = "email";
    private static final String key_sesso = "sesso";

   // private static final String URL = "http://10.0.2.2/servizi/registrazione";

    private static final String URL = "http://192.168.1.53/servizi/registrazione";


    private RequestQueue codaRichiesta = null;





    public registerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final   View view = inflater.inflate(R.layout.fragment_register, container, false);


        this.radioSexGroup = (RadioGroup) view.findViewById(R.id.radioSex);
        this.conferma = (Button) view.findViewById(R.id.RegisterBtn);
       this.indietro = (Button) view.findViewById(R.id.indietrorgs);
        this.username = (EditText) view.findViewById(R.id.Username);
        this.nome =  (EditText) view.findViewById(R.id.Nome);
        this.confermaPassword = (EditText) view.findViewById(R.id.conferma_password);
        this.password = (EditText) view.findViewById(R.id.password);
        this.cognome = (EditText) view.findViewById(R.id.Cognome);
        this.email = (EditText) view.findViewById(R.id.Email);



indietro.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        FragmentManager f1 = getFragmentManager();
        android.app.FragmentTransaction fragmentTransaction = f1.beginTransaction();
        fragmentTransaction.hide(registerFragment.this);
        welcomeFragment welcomeFragment = new welcomeFragment();
        fragmentTransaction.add(R.id.welcome_frame, welcomeFragment);
        fragmentTransaction.commit();

    }
});


conferma.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        int selectedId = radioSexGroup.getCheckedRadioButtonId();
        radioSexButton = (RadioButton) view.findViewById(selectedId);//quiando premo su conferma si memorizza il valore selezionato in lista
        final String radiovalue = radioSexButton.getText().toString().trim();

        final String nome_string = nome.getText().toString().trim();
        final String cognome_string = cognome.getText().toString().trim();
        final String email_string = email.getText().toString().trim();
        final String username_string = username.getText().toString().trim();
        final String password_string = password.getText().toString().trim();
        final String password_confermed_string = confermaPassword.getText().toString().trim();

        if (username_string.equals("") || password_string.equals("")|| email_string.equals("") || nome_string.equals("") || cognome_string.equals("") ) {

            Toast.makeText(getActivity(), "Riempi tutti i campi", Toast.LENGTH_LONG).show();

        } else {
            if( password_confermed_string.equals(password_string)){
                StringRequest richiesta = new StringRequest(Request.Method.POST, URL,

                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                if (response.trim().equals("errore")) {
                                    Toast.makeText(getActivity(), "Impossibile registrare l'utente", Toast.LENGTH_LONG).show();


                                } else {
                                    Toast.makeText(getActivity(), "Registrazione riuscita", Toast.LENGTH_LONG).show();

                                    FragmentManager f1 = getFragmentManager();
                                    android.app.FragmentTransaction fragmentTransaction = f1.beginTransaction();
                                    fragmentTransaction.hide(registerFragment.this);
                                    welcomeFragment Welcome = new welcomeFragment();
                                    fragmentTransaction.add(R.id.welcome_frame, Welcome);
                                    fragmentTransaction.commit();



                                }


                            }
                        },

                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getActivity(), "Errore di connessione", Toast.LENGTH_LONG).show();
                            }
                        }
                )
                {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                        map.put(key_username, username_string);//inserisco i nuovi valori in corrispondenza delle loro chiavi
                        map.put(key_password, password_string);
                        map.put(key_nome, nome_string);
                        map.put(key_cognome, cognome_string);
                        map.put(key_email, email_string);
                        map.put(key_sesso, radiovalue);
                    return map;
                }
                };

                codaRichiesta = Volley.newRequestQueue(getActivity());
                codaRichiesta.add(richiesta);

            }else{
                Toast.makeText(getActivity(), "La password non corrisponde", Toast.LENGTH_LONG).show();
            }
        }


    }
});

        return view;



    }

}
