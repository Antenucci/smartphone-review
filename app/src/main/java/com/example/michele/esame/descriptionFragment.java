package com.example.michele.esame;


import android.app.FragmentManager;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


/**
 * A simple {@link Fragment} subclass.
 */
public class descriptionFragment extends Fragment {


    public descriptionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
    final View view = inflater.inflate(R.layout.fragment_description, container, false);

        final TextView nometxt = (TextView) view.findViewById(R.id.nome_smartphone);
        final TextView marcatxt = (TextView) view.findViewById (R.id.marca_smartphone);
        final TextView desctxt = (TextView) view.findViewById (R.id.descrizione_smartphone);
        final ImageView img = (ImageView) view.findViewById(R.id.img_smartphone1);
        final Button commenti = (Button)  view.findViewById(R.id.commentiBtn);



        String nome = getActivity().getIntent().getStringExtra("nome");
        String marca =  getActivity().getIntent().getStringExtra("marca");
        String desc =  getActivity().getIntent().getStringExtra("descrizione");
        String imgview =  getActivity().getIntent().getStringExtra("immagine");


        Picasso.with(getActivity()).load(imgview).into(img);
        nometxt.setText("Nome smartphone: "+nome);
        marcatxt.setText("Marca: "+marca);
        desctxt.setText("Descrizione: "+desc);


        commenti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager f1 = getFragmentManager();
                android.app.FragmentTransaction fragmentTransaction = f1.beginTransaction();
                fragmentTransaction.hide(descriptionFragment.this);
                commentFragment commentifragment = new commentFragment();
                fragmentTransaction.add(R.id.comment_frame, commentifragment);
                fragmentTransaction.commit();

            }
        });



        return view;

    }

}
